﻿using RazorHosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorHostingConsoleSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Starting...");
            Run();
        }
        private static void Run()
        {
            var preview = new Preview();
            var currentThreadID = Environment.CurrentManagedThreadId;
            Process.GetCurrentProcess().WaitForExit(-1);
        }

    }
}
