﻿using RazorHosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace RazorHostingConsoleSamples
{
    public class Preview
    {
        /// <summary>
        /// Cached instance of RazorHost
        /// </summary>
        RazorEngine<RazorTemplateBase> Host { get; set; }

        /// <summary>
        /// A custom user context object we pass to the Template
        /// </summary>
        public CustomContext CustomContext { get; set; }

        public string FileTemplatePath { get; set; }
        public string txtSource { get; set; }
        public string txtResult { get; set; }
        public ComponentResourceManager resources { get; set; }
        public string result = "";
        public Preview()
        {
            // Create a custom context we can pass to Template
            CustomContext = new CustomContext();
            // Assign a reference to this form for use within
            // the template
            CustomContext.Instance = this;

            //resources = new ComponentResourceManager(typeof(RazorScriptingSampleForm));

            //this.txtSource = "welcome";
            this.txtSource = @"Hello @Context.GetFullName(). Entered time is: @Context.Entered.
Current time is: @DateTime.Now
This Template runs in its own AppDomain which can be unloaded.
Assembly: @System.Reflection.Assembly.GetExecutingAssembly().FullName
";

            RazorEngine<RazorTemplateBase> host = CreateHost();

            if (host == null)
                return;
            TextReader reader = new StringReader(this.txtSource);
            string assemblyId = host.ParseAndCompileTemplate(new string[] { "System.Windows.Forms.dll", "Westwind.Utilities.dll" },
                reader, "__RazorHost", null);
            //string result = host.ExecuteTemplateByAssembly(assemblyId,
            //        "RazorTest", "RazorTemplate",
            //        // Pass in this WinForm as a Context so we can
            //        // manipulate the form from within Razor code
            //        this.CustomContext);


            string result = host.RenderTemplate(
                    this.txtSource.ToString(),
                    new string[] { "System.Windows.Forms.dll", "System.Web.dll" },
                    this.CustomContext);

            if (result == null)
            {
                MessageBox.Show(host.ErrorMessage, "Template Execution Error");
                return;
            }

            this.txtResult = result;
            MessageBox.Show("Template Execution: ", result);
            var engine = new RazorEngine<RazorTemplateBase>();

            // we can pass any object as context - here create a custom context
            var context = new CustomContext()
            {
                Instance = this,
                Entered = DateTime.Now.AddDays(-10)
            };

            string output = engine.RenderTemplate(this.txtSource,
                                                  new string[] { "System.Windows.Forms.dll" },
                                                  context);
            MessageBox.Show("Template Execution: ", output);

        }

        private RazorEngine<RazorTemplateBase> CreateHost()
        {
            if (this.Host != null)
                return this.Host;

            // Use Static Methods - no error message if host doesn't load
            this.Host = RazorEngineFactory<RazorTemplateBase>.CreateRazorHost();
            //this.Host = RazorEngineFactory<RazorTemplateBase>.CreateRazorHostInAppDomain();
            if (this.Host == null)
            {
                MessageBox.Show("Unable to load Razor Template Host","Razor Hosting");
            }

            // Use instance methods
            //RazorEngineFactory<RazorTemplateBase> factory = new RazorEngineactory<RazorTemplateBase>();
            //this.Host = factory.GetRazorHostInAppDomain();
            //if (this.Host == null)
            //{
            //    MessageBox.Show("Unable to load Razor Template Host\r\n" + factory.ErrorMessage,
            //                    "Razor Hosting", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);             
            //}            

            return this.Host;
            throw new NotImplementedException();
        }

    }
}
