﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorHostingConsoleSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Starting...");
            Run();
        }
        private static void Run()
        {
            System.Console.WriteLine("Preloading assemblies...");
            ReflectionUtils.PreloadAssemblies(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

            var role = new WorkerRole();
            role.Run();
        }

    }
}
